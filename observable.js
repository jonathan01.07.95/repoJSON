const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');

//por que tiene signo de dolar al final 
// of --> me crea un STRING 
const observableEjemplo$ = of(1,2,2,3,4,5,6,7)
//recibe tres callback, respuest del observable, el error y la informacion que termino le obervable 
const respuestaObservable =(respuesta)=>{
    console.log('Respuesta Observable',respuesta)
}
const errorObservable =(errorObservable)=>{
    console.log(errorObservable)
}
const terminoObservable=()=>{
    console.log('Termino Observable')
}

function sumarUno(numero){
    return numero + 1
}

function multiplicarDos(numero){
    return numero*2
}


// crear promesa 
const promesaResta = (numero) =>{
    const funcionPromesa = (resolve, reject) => {
        resolve(numero - 1)
    }

    return new Promise(funcionPromesa)
}

const observableResta = (numero) =>{    
    const promesaObservableConvertido$ = from(promesaResta(numero))
    return promesaObservableConvertido$
}

observableEjemplo$
    .pipe(//tiene una concatenacion funciones---> que tipo de operadores
        map(sumarUno),
        map(multiplicarDos),
        //distinct para eliminar duplicados
        distinct(),
        mergeMap(observableResta)
    )
    .subscribe(
        respuestaObservable,
        errorObservable,
        terminoObservable
    )
//encadenar o cocatenar funciones pipe

