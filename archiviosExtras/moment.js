var moment = require('moment');

moment.locale('es');
var fecha = moment() 
console.log('moment', fecha)

//Obtener año con moment

var anio = fecha.get('year')
var anio = fecha.year()
console.log('año',anio)

//Obtener mes con moment

var mes = fecha.get('month')
var mes = fecha.month()
console.log('mes',mes)

//Obtener dia con moment

var dia = fecha.get('date')
var dia = fecha.date()
console.log('dia',dia)

var jsonFecha = {
    year: 2010,
    month:3,
    date:5
    
}
console.log(moment(jsonFecha))

var fechaInicio = moment()
var fechaFin = moment('2018-12-24')
var diferencia = fechaInicio.diff(fechaFin, 'days')
var diferencia2 = fechaFin.diff(fechaInicio,'seconds')
console.log(diferencia)
console.log(diferencia2)

console.log('Fecha a objeto',moment().toObject())
console.log('Fecha UTC',moment().utc())

//Crear un arreglo con un atributo nombre y fechas de nacimiento randomico desde 1930 al 2018
// Calcular la edad en una funcion
// Filtrar a usuarios menores de edad
// Filtrar adultos
// Filtrar adultos mayores
