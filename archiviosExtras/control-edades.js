var random = require('random-js')();
var moment = require('moment');

var personas = [
    {Nombre: 'Ricardo'},
    {Nombre: 'Carlos'},
    {Nombre: 'Marco'},
    {Nombre: 'Lisseth'},
    {Nombre: 'Erika'},
    {Nombre: 'Leonardo'},
    {Nombre: 'Alan'},
    {Nombre: 'Leimy'},
    {Nombre: 'Karla'},
    {Nombre: 'Veronica'},
]


var edadesRandomicas = personas.map((valor)=>{
    valor.fechaNacimiento = random.integer(1930, 2018);
    valor.edad = calcularEdad(valor.fechaNacimiento);
    return valor
    
})
console.log(personas)

function calcularEdad(fechaNacimiento){
    var fechaActual = moment().year()
    return fechaActual-fechaNacimiento
}
