const funcionSaludar= require('./saludar')

function peticionUsuario(cb,saludo){ //si es sincrono se utiliza el try y catch
cb(saludo)   // si es asincrono no se utiliza el try 

console.log('Saludo desde el callback')
}

peticionUsuario(funcionSaludar, 'Este es el nuevo saludo')

const leerArchivo = require('./leer-archivo.js')

const archivo={
    path:'texto21.txt',
    informacionArchivoSiNoExiste: 'Se crea con esta informacion',
    nombreNuevo: archivo.path
}

leerArchivo('texto1.txt',(objeto,resultadoPeticion)=>{
  if(resultadoPeticion.crearArchivo){
    console.log('El archivo no existia y se creo con esta date',resultadoPeticion )
  }
  else{
    console.log('Leyo archivo y tenia esta data',resultadoPeticion )
  }
    
    //console.log('Resultado:',resultado)
})


// Crear arreglo de usuarios con atributo nombre y apellido
// voy a tener una funcion buscar y una para crear
// y otra para buscar o crear 
// Debe aparecer se creo el usuario "Pepito perez" 
