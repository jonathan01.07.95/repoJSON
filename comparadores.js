
/*
and &&
or ||
equals 
>
<
=
!=
=    -> asignacion
==   ->comparar valor, contenido
===  ->comparar valor y tipo de variable
*/

// mitad y cerca de terminar
// 
/*
var valor = 100;
var condicionActual = valor > 0
var whileMitad = valor === 50
var whileTerminar = valor === 10

while (condicionActual) {
    console.log(`${valor}`)
    if(whileMitad){
        console.log(`el while esta por la mitad`)
    }
    else if(whileTerminar){
        console.log(`el while esta pronto a terminar`)
    }
    --valor
    condicionActual = valor > 0
    whileMitad = valor === 50
    whileTerminar = valor === 10
}
*/

var valor = 100;
var condicionActual = valor > 0
var whileMitad = valor === 48
var whileTerminar = valor === 10

while (valor > 0) {
    console.log(`${valor}`)
    if(whileMitad){
        console.log(`el while esta por la mitad`)
    }
    else if(whileTerminar){
        console.log(`el while esta pronto a terminar`)
    }
    valor-4
}
