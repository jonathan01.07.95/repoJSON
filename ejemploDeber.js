var menu = require('console-menu');
var readlineSync = require('readline-sync')
const misUsuarios = []
var usuariosCreados = []
var FechasYDescuentos = [
    {
        fechaIncio: '2018-08-25',
        descuento: '30%',
        fechaFin: '2018-08-26',
    },
    {
        fechaIncio: '2018-08-28',
        descuento: '20%',
        fechaFin: '2018-08-29',
    },
    {
        fechaIncio: '2018-09-01',
        descuento: '25%',
        fechaFin: '2018-09-02',
    },
    {
        fechaIncio: '2018-09-04',
        descuento: '40%',
        fechaFin: '2018-09-05',
    }
]
var Proveedores = [
    {
        nombre: 'Tia',
        descuentos: FechasYDescuentos
    },
    {
        nombre: 'Santa Maria',
        descuentos: FechasYDescuentos
    },
    {
        nombre: 'SuperMaxi',
        descuentos: FechasYDescuentos
    }
]
var objetoReporte = ''
var reportes = []
//var nuevoUsuario = {}




const menuPrincipal = (usuarios, ListaReportes) => {
    const opcionesDelMenu = [
        { hotkey: 1, title: 'Logear Usuario' },
        { hotkey: 2, title: 'Crear Usuario', data: { estadoDeLogueo: false } }
    ]

    const configuracionesDelMenu = {
        header: 'Menu Principal',
        border: true
    }

    if (usuarios) {
        opcionesDelMenu[2] = { hotkey: 3, title: 'Listar Usuarios' }
    }
    if (ListaReportes) {
        opcionesDelMenu[3] = { hotkey: 4, title: 'Reportes' }
    }

    return menu(opcionesDelMenu, configuracionesDelMenu)
        .then((item) => {
            switch (item.hotkey) {
                case 1:
                    if (existenElemetos(usuariosCreados)) {
                        seleccionDeUsuarioAIngresar(usuariosCreados)
                    } else {
                        menuPrincipal()
                    }
                    break;
                case 2:
                    readlineSync.promptCL(function (userName) {
                        let nuevoUsuario = {}
                        nuevoUsuario.nombre = userName
                        nuevoUsuario.estadoDeLogueo = item.data.estadoDeLogueo
                        crearUsuario(nuevoUsuario, ListaReportes)
                    }); 
                    break;
                case 3:
                    funcionDeListar(usuarios)
                    menuPrincipal(usuarios, ListaReportes)
                    break
                case 4:
                    funcionDeListar(reportes)
                    menuPrincipal(usuarios, ListaReportes)
                    break
                default:
                    console.log('Opcion invalida');
            }
        })
}

const seleccionDeUsuarioAIngresar = (usuarios) => {
    let usuariosDisponibles = []
    usuarios.forEach((valor, index) => {
        let objetoUsuario = {
            hotkey: index + 1,
            title: valor.nombre,
            estado: valor.estadoDeLogueo
        }
        usuariosDisponibles.push(objetoUsuario)
    });
    const configuracionesDelMenu = {
        header: 'Seleccione el Usuario',
        border: true
    }

    return menu(usuariosDisponibles, configuracionesDelMenu)
        .then((item) => {
            let indice = 1
            while (indice <= usuariosDisponibles.length) {
                switch (item.hotkey) {
                    case indice:
                        logearUsuario(item)
                        break;
                }
                indice++
            }
        })
}

const menuUsuarioLogueado = (proveedores, usuario) => {
    const opcionesDelMenu = [
        { hotkey: 1, title: 'Selecciones un Proveedor' },
        { hotkey: 2, title: 'Crear Proveedor', data: { nombre: 'Multicines', descuentos: FechasYDescuentos } }
    ]

    const configuracionesDelMenu = {
        header: 'Seleccione una Opcion',
        border: true
    }

    return menu(opcionesDelMenu, configuracionesDelMenu)
        .then(item => {
            switch (item.hotkey) {
                case 1:
                    seleccionDeProveedor(proveedores, usuario)
                    break
                case 2:
                    crearProveedor(item.data, usuario)
                    break
                default: console.log('404')

            }
        })
}

const seleccionDeProveedor = (proveedores, usuario) => {
    let proveedoresDisponibles = []
    proveedores.forEach((valor, index) => {
        let objetoProveedor = {
            hotkey: index + 1,
            title: valor.nombre,
            descuentos: valor.descuentos
        }
        proveedoresDisponibles.push(objetoProveedor)
    });
    const configuracionesDelMenu = {
        header: 'Seleccione un Proveedor',
        border: true
    }

    return menu(proveedoresDisponibles, configuracionesDelMenu)
        .then((item) => {
            let indice = 1
            while (indice <= proveedoresDisponibles.length) {
                switch (item.hotkey) {
                    case indice:
                        objetoReporte += `, Proveedor: ${item.title}`
                        menuProveedor(item, usuario)
                        break;
                }
                indice++
            }
        })
}

const menuProveedor = (proveedor, usuario) => {
    const opcionesDelMenu = [
        { hotkey: 1, title: 'Selecciones un Descuento' },
        { hotkey: 2, title: 'Crear Descuento', data: { fechaIncio: '2018-08-25', descuento: '80%', fechaFin: '2018-08-26' } }
    ]

    const configuracionesDelMenu = {
        header: 'Seleccione una Opcion',
        border: true
    }

    return menu(opcionesDelMenu, configuracionesDelMenu)
        .then(item => {
            switch (item.hotkey) {
                case 1:
                    console.log('opcion 1')
                    seleccionDelDescuento(proveedor.descuentos, usuario)
                    break
                case 2:
                    console.log('opcion 2')
                    console.log(proveedor.title)
                    crearDescuento(item.data, proveedor, usuario)
                    break
                default: console.log('404')

            }
        })
}

const seleccionDelDescuento = (descuentos, usuario) => {
    let descuentosDisponibles = []
    descuentos.forEach((valor, index) => {
        let objetoDescuento = {
            hotkey: index + 1,
            title: valor.descuento + '| VALIDA: ' + valor.fechaIncio + '  HASTA:' + valor.fechaFin,
            fechaIncio: valor.fechaIncio,
            fechaFin: valor.fechaFin
        }
        descuentosDisponibles.push(objetoDescuento)
    });
    const configuracionesDelMenu = {
        header: 'Seleccione el descuento',
        border: true
    }

    return menu(descuentosDisponibles, configuracionesDelMenu)
        .then((item) => {
            let indice = 1
            while (indice <= descuentosDisponibles.length) {
                switch (item.hotkey) {
                    case indice:
                        objetoReporte += `, Descuento: ${item.title}`
                        reportes.push(objetoReporte)
                        usuario.estado = false
                        menuPrincipal(usuariosCreados, usuario) //usuariosCreados,
                        break;
                }
                indice++
            }
        })
}





// creacion de elemetos
const crearUsuario = (usuario, ListaReportes) => {
    usuariosCreados.push(usuario);
    if (existenElemetos(usuariosCreados)) {
        menuPrincipal(usuariosCreados, ListaReportes)
    } else {
        menuPrincipal()
    }


}

const crearProveedor = (proveedor, usuario) => {
    Proveedores.push(proveedor);
    if (existenElemetos(Proveedores)) {
        menuUsuarioLogueado(Proveedores, usuario)
    } else {
        menuUsuarioLogueado()
    }


}

const crearDescuento = (descuento, Proveedor, usuario) => {
    Proveedores[Proveedor.hotkey - 1].descuentos.push(descuento)
    if (existenElemetos(Proveedores[Proveedor.hotkey - 1].descuentos)) {
        menuProveedor(Proveedor, usuario)
    } else {
        menuProveedor()
    }


}

// logue
const logearUsuario = (usuario) => {
    console.log('antes', objetoReporte)    // mejoara
    usuario.estado = true
    objetoReporte = `Nombre: ${usuario.title}`
    console.log('despues', objetoReporte)
    if (existenElemetos(Proveedores)) {
        menuUsuarioLogueado(Proveedores, usuario)
    } else {
        menuUsuarioLogueado()
    }
}


// funciones del vector
var funcionDeListar = (arreglo) => {
    arreglo.forEach((valor) => {
        console.log(valor)
    })
}

var existenElemetos = (arreglo) => {
    let hayElemetos = arreglo.length > 0
    if (hayElemetos) {
        return true
    } else {
        return false
    }
}

menuPrincipal()
