const EventEmitter = require('events');

/*class EntrarALaCasa extends EventEmitter{

}

const entrarALaCasa = new EntrarALaCasa();

entrarALaCasa.on("entro por la puerta delantera", (nombre)=>{
    console.log(`saludar a ${nombre}`, "que ingreso por la puerta delantera")
})

entrarALaCasa.on("entro por la puerta trasera", (nombre)=>{
    console.log(`saludar a ${nombre}`, "que ingreso por la puerta trasera")
})

entrarALaCasa.on("entro por la ventana", ()=>{
    console.log("Cuidado alguien entro por la ventana")
})

entrarALaCasa.emit("entro por la puerta delantera", "Jonathan")
entrarALaCasa.emit("entro por la puerta trasera", "Jonathan")
entrarALaCasa.emit("entro por la ventana", "Jonathan")
*/

///////////////////////////////////////////////////////////////

class UsarCelular extends EventEmitter{

}

const usarCelular = new UsarCelular();

usarCelular.on("realizar una llamada", (nombre)=>{
    console.log(`${nombre} realizara una llamada desde su celular`)
})

usarCelular.on("revisar whatsapp", (nombre)=>{
    console.log(`${nombre} respondera un mensaje por whatsapp`)
})

usarCelular.on("ver memes", (nombre)=>{
    console.log(`${nombre} mirara memes para ponerse feliz`)
})

usarCelular.on("tomar una foto", (nombre)=>{
    console.log(`${nombre} tomara una foto para tener un recuerdo`)
})

usarCelular.emit("realizar una llamada", "Jonathan")
usarCelular.emit("revisar whatsapp", "Jonathan")
usarCelular.emit("ver memes", "Jonathan")
usarCelular.emit("tomar una foto", "Jonathan")