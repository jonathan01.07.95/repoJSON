
const fs = require('fs')

module.exports = (path, cb) => {
    fs.readFile(path, 'utf8', (errorLectura, contenidoArchivo)=>{
        if(errorLectura){
            fs.writeFile(path, 'Hola que hace XD',(errorCreacion)=>{
                if(errorCreacion){
                    cb({
                        error: errorLectura,
                        nombreArchivo: path,
                        informacion : contenidoArchivo,
                        archivoCreado : true, 
                    }, 'documento creado')
                }
            })
        }    
        else{
            cb({
                error: errorLectura,
                informacion : contenidoArchivo,
                tieneError : false
            }, 'string sin error creo')
        }
    })

}