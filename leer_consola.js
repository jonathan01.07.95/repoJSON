var readlineSync = require('readline-sync');
var menu = require('console-menu');

///var userName = readlineSync.question('May I have your name? ');
//console.log('Hi ' + userName + '!');

const arregloUsuario = []

const registrarUsuario = function (datos) {
    var  nombreUsuarioNuevo = readlineSync.question('Ingrese el nombre del nuevo usuario: ')
    console.log('El usuario que se registro fue: '+ nombreUsuarioNuevo)
    arregloUsuario.push(nombreUsuarioNuevo);
    const hayUsuarios = arregloUsuario.length > 0

    if(hayUsuarios){
        ejecutarMenu(true)
    }else {
        ejecutarMenu()
    }

}

const logearUsuario = function (datos) {
    //console.log('Datos ', datos);
    console.log(arregloUsuario);
    seleccionarUsuario(arregloUsuario)
}

const seleccionarUsuario = (usuarios)=>{
    const hayUsuarios = usuarios.length > 0

    if(hayUsuarios){
        ejecutarMenuUsuarios(usuarios)
    }else {
        ejecutarMenu()
    }
}

const ejecutarMenu = (agregarOpcion) => {

    const menuSeleccion =[
        { hotkey: '1', title: 'Crear usuario' },
        { hotkey: '2', title: 'Logear usuario' }
    ]

    return menu(menuSeleccion, {
        header: 'Menu',
        border: true,
    })
    .then(item => {
        switch (item.hotkey) {
            case '1':
                console.log('Opcion 1');
                registrarUsuario(item.data);
                break;
            case '2':
                console.log('Opcion 2');
                logearUsuario(item.data);
                break;
                case '3':
                console.log('Opcion 3');
                ejecutarMenuUsuarios(arregloUsuario)
                break;
            default:
                console.log('Opcion invalida');
        }
    });
}

const ejecutarMenuUsuarios = (usuarios) => {

    let usuarios2=[]
    usuarios.forEach((valor,indice) => {
        let objetoMenu={
        hotkey:indice,
        title:valor.nombre
       }
       usuarios2.push(objetoMenu)
   });

   
    console.log('usuarios2', usuarios2)
    return menu(usuarios2, {
            header: 'Menu',
            border: true,
        })
        .then(item => {
            switch (item.hotkey) {
                case '1':
                    console.log('Opcion 1');
                    registrarUsuario(item.data);
                    break;
                case '2':
                    console.log('Opcion 2');
                    logearUsuario(item.data);
                    break;
                default:
                    console.log('Opcion invalida');
            }
        });
}


ejecutarMenu();