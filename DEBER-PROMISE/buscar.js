module.exports = (arregloUsuarios, usuarioABuscar)=>{
        let usuarioEncontrado = arregloUsuarios.find((usuario)=>{
            return usuario === usuarioABuscar
        })
        const promesa = new Promise((resolve, reject)=>{ 
            if(usuarioEncontrado){
                resolve({
                    mensaje: 'usuario encontrado',
                    usuarioEncontrado
                })
            }
            else{
                reject({
                    mensaje: 'usuario no encontrado'
                })
            }
        })
    return promesa
}