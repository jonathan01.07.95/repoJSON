const crear = require('./crear.js') //Cualquiera de las dos maneras 
const buscar = require('./buscar.js')
const elimnar = require('./elimnar.js')
const buscar0Crear = require('./buscarCrear.js')
const usuarios = require('./datosUsuarios.js')

module.exports = {
    crear,
    buscar,
    elimnar,
    buscar0Crear,
    usuarios
}