const paquetes = require('./paquetes.js');

module.exports = (arreglo, usuarioAEliminar)=>{
    const promesa = new Promise((resolve, reject)=>{     
        paquetes.buscar((arreglo, usuarioAEliminar)=>{
            let existeElUsuario = (usuario => (usuario.nombre === usuarioAEliminar.nombre))
            const indiceDelElementoABorrar = arreglo.findIndex(existeElUsuario)
            arreglo.splice(indiceDelElementoABorrar,1)
        })
        .then(()=>{
                resolve({
                    mensaje: 'se elimino el usuario'
                })
            })
        .catch(()=>{
            reject({
                mensaje: 'no se elimino usuario'
            })
        })
    })    
    return promesa
}


