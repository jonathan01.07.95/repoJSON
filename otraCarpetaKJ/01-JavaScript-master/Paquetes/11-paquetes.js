const saludar = require('./saludar')
const despedir = require('./despertar')
const sumar = require('./sumar')
const division = require('./division')
const restar = require('./restar')
const multiplicar = require('./multiplicar')

module.exports = {
    saludar,
    despedir,
    multiplicar,
    sumar,
    restar,
    division
}
