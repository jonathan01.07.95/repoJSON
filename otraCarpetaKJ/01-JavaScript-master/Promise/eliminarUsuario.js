module.exports = (arreglo,usuarioAEliminar)=>{
    let existeElUsuario = (usuario => (usuario.nombre === usuarioAEliminar.nombre && usuario.apellido === usuarioAEliminar.apellido))
    const indiceDelElementoABorrar = arreglo.findIndex(existeElUsuario)
    arreglo.splice(indiceDelElementoABorrar,1)
    return new Promise((resolve,reject)=>{
        reject({
            mensaje: 'no se pudo eliminar',            
        })
        resolve({
            mensaje: 'se elimino con exito',
            arreglo
        })    
    })        
}



