

module.exports = (arreglo,usuarioABuscar)=>{
    
    
    const usuarioEncontrado = arreglo.find(usuario=>{   
        let existeElUsuario = (usuario.nombre === usuarioABuscar.nombre&& usuario.apellido === usuarioABuscar.apellido)                
        return existeElUsuario
    })
    return new Promise((resolve,reject)=>{
        if (usuarioEncontrado) {
            resolve({
                mensaje: 'usuario encontrado',
                usuarioEncontrado
            })  
        }else{
            reject({
                mensaje: 'usuario no encontrado',
                usuarioEncontrado
            })      
        }          
    })
}