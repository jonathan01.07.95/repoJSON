var menu = require('console-menu');


const ejecutarMenuPrincipal = (agregarOpcion) => {

    const menuSeleccion =[
        { hotkey: '1', title: 'Crear usuario', data: { nombre: 'Adrian' } },
        { hotkey: '2', title: 'Logear usuario', data: { nombre: 'Vicente' } }
    ]

    const configuracionMenuPrincipal = {
        header: 'Menu principal',
        border: true,
    }


    if(agregarOpcion){
        //menuSeleccion[2]= { hotkey: '3', title: 'Listar usuarios', data: { nombre: 'Adrian' }}
        menuSeleccion.push({ hotkey: '3', title: 'Listar usuarios', data: { nombre: 'Adrian' }})
    }


    return menu(menuSeleccion, configuracionMenuPrincipal )
        .then(itemSeleccionado => {
            switch (itemSeleccionado.hotkey) {
                case '1':
                    console.log('Opcion 1');
                    ejecutarMenuPrincipal()
                   // registrarUsuario(itemSeleccionado.data);
                    break;
                case '2':
                    console.log('Opcion 2');
                    ejecutarMenuPrincipal()
                   // logearUsuario(itemSeleccionado.data);
                    break;
                case '3':
                    console.log('Opcion 3');
                   // ejecutarMenuUsuarios(arregloUsuario)
                    break;
                default:
                    console.log('Opcion invalida');
            }
        });
}

ejecutarMenuPrincipal(true)