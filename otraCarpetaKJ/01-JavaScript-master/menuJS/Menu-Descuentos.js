var menu = require('console-menu');
var readlineSync = require('readline-sync')
var usuariosCreados = [
    {
        nombre: 'kevin',
        estadoDeLogueo: false
    },
    {
        nombre: 'orlando',
        estadoDeLogueo: false
    },
    {
        nombre: 'juan',
        estadoDeLogueo: false
    }
]
var misUsuarios = []
var FechasTia = [
    {
        fechaIncio: '2018-08-25',        
        fechaFin: '2018-08-26',
        descuentos: ['50%']
    },
    {
        fechaIncio: '2018-08-28',        
        fechaFin: '2018-08-29',
        descuentos: ['50%']
    },
    {
        fechaIncio: '2018-09-01',        
        fechaFin: '2018-09-02',
        descuentos: ['50%']
    },
    {
        fechaIncio: '2018-09-04',        
        fechaFin: '2018-09-05',
        descuentos: ['50%']
    }
]
var FechasSantaMaria = [
    {
        fechaIncio: '2018-10-05',        
        fechaFin: '2018-10-06',
        descuentos: ['10%']
    },
    {
        fechaIncio: '2018-09-28',        
        fechaFin: '2018-09-29',
        descuentos: ['10%']
    },
    {
        fechaIncio: '2018-11-13',        
        fechaFin: '2018-11-14',
        descuentos: ['10%']
    },
    {
        fechaIncio: '2018-12-14',        
        fechaFin: '2018-12-15',
        descuentos: ['10%']
    }
]
var FechasSuperMaxi = [
    {
        fechaIncio: '2018-08-30',        
        fechaFin: '2018-08-31',
        descuentos: ['30%']
    },
    {
        fechaIncio: '2018-09-28',        
        fechaFin: '2018-09-29',
        descuentos: ['30%']
    },
    {
        fechaIncio: '2018-10-10',        
        fechaFin: '2018-10-15',
        descuentos: ['30%']
    },
    {
        fechaIncio: '2018-10-20',        
        fechaFin: '2018-10-23',
        descuentos: ['30%']
    }
]
var Proveedores = [
    {
        nombre: 'Tia',
        descuentos: FechasTia
    },
    {
        nombre: 'Santa Maria',
        descuentos: FechasSantaMaria
    },
    {
        nombre: 'SuperMaxi',
        descuentos: FechasSuperMaxi
    }
]
var misProveedores = []
var objetoReporte = ''
var reportes = []
//var nuevoUsuario = {}




const menuPrincipal = (usuarios, ListaReportes) => {
    const opcionesDelMenu = [
        { hotkey: 1, title: 'Logear Usuario' },
        { hotkey: 2, title: 'Crear Usuario', data: { estadoDeLogueo: false } }
    ]

    const configuracionesDelMenu = {
        header: 'Menu Principal',
        border: true
    }

    if (usuarios) {
        opcionesDelMenu[2] = { hotkey: 3, title: 'Listar Usuarios' }
    }
    if (ListaReportes) {
        opcionesDelMenu[3] = { hotkey: 4, title: 'Reportes' }
    }

    return menu(opcionesDelMenu, configuracionesDelMenu)
        .then((item) => {
            switch (item.hotkey) {
                case 1:
                    if (existenElemetos(misUsuarios)) {
                        seleccionDeUsuarioAIngresar(misUsuarios)
                    } else {
                        menuPrincipal()
                    }
                    break;
                case 2:                    
                    crearUsuario(usuariosCreados, ListaReportes)                     
                    break;
                case 3:
                    funcionDeListar(usuarios)
                    menuPrincipal(usuarios, ListaReportes)
                    break
                case 4:
                    funcionDeListar(reportes)
                    menuPrincipal(usuarios, ListaReportes)
                    break
                default:
                    console.log('Opcion invalida');
            }
        })
}

const seleccionDeUsuarioAIngresar = (usuarios) => {
    let usuariosDisponibles = []
    usuarios.forEach((valor, index) => {
        let objetoUsuario = {
            hotkey: index + 1,
            title: valor.nombre,
            estado: valor.estadoDeLogueo
        }
        usuariosDisponibles.push(objetoUsuario)
    });
    const configuracionesDelMenu = {
        header: 'Seleccione el Usuario',
        border: true
    }

    return menu(usuariosDisponibles, configuracionesDelMenu)
        .then((item) => {
            let indice = 1
            while (indice <= usuariosDisponibles.length) {
                switch (item.hotkey) {
                    case indice:
                    console.log('ddd ',item)
                        logearUsuario(item)
                        break;
                }
                indice++
            }
        })
}

const menuUsuarioLogueado = (proveedores, usuario) => {
    const opcionesDelMenu = [
        { hotkey: 1, title: 'Selecciones un Proveedor' },
        { hotkey: 2, title: 'Crear Proveedor'}
    ]

    const configuracionesDelMenu = {
        header: 'Seleccione una Opcion',
        border: true
    }
    

    return menu(opcionesDelMenu, configuracionesDelMenu)
        .then(item => {                        
            switch (item.hotkey) {
                case 1:
                    if(existenElemetos(proveedores)){
                        seleccionDeProveedor(proveedores, usuario)                                          
                    }else{
                        menuUsuarioLogueado()                        
                    }                                    
                    break
                case 2:
                console.log(Proveedores)
                    crearProveedor(Proveedores, usuario)
                    break
                default: console.log('404')

            }
        })
}

const seleccionDeProveedor = (proveedores, usuario) => {
    let proveedoresDisponibles = []
    proveedores.forEach((valor, index) => {
        let objetoProveedor = {
            hotkey: index + 1,
            title: valor.nombre,
            descuentos: valor.descuentos
        }
        proveedoresDisponibles.push(objetoProveedor)
    });
    const configuracionesDelMenu = {
        header: 'Seleccione un Proveedor',
        border: true
    }

    return menu(proveedoresDisponibles, configuracionesDelMenu)
        .then((item) => {
            let indice = 1
            while (indice <= proveedoresDisponibles.length) {
                switch (item.hotkey) {
                    case indice:
                    console.log(item.title)
                    console.log(usuario)
                        objetoReporte += `, Proveedor: ${item.title}`
                        menuProveedor(item, usuario)
                        break;
                }
                indice++
            }
        })
}

const menuProveedor = (proveedor, usuario) => {
    const opcionesDelMenu = [
        { hotkey: 1, title: 'Selecciones un Descuento' },
        { hotkey: 2, title: 'Crear Descuento'}
    ]

    const configuracionesDelMenu = {
        header: 'Seleccione una Opcion',
        border: true
    }
    console.log('proveedor '+proveedor+' usuario '+ usuario.title)

    return menu(opcionesDelMenu, configuracionesDelMenu)
        .then(item => {
            ////// falat arreglar esta parte
            switch (item.hotkey) {
                case 1:
                    console.log('opcion 1')
                    seleccionDelDescuento(proveedor.descuentos, usuario)
                    break
                case 2:
                    console.log('opcion 2')                    
                    crearDescuento(item.data, proveedor, usuario)
                    break
                default: console.log('404')

            }
        })
}

const seleccionDelDescuento = (descuentos, usuario) => {
    let descuentosDisponibles = []
    descuentos.forEach((valor, index) => {
        let objetoDescuento = {
            hotkey: index + 1,
            title: valor.descuento + '| VALIDA: ' + valor.fechaIncio + '  HASTA:' + valor.fechaFin,
            fechaIncio: valor.fechaIncio,
            fechaFin: valor.fechaFin
        }
        descuentosDisponibles.push(objetoDescuento)
    });
    const configuracionesDelMenu = {
        header: 'Seleccione el descuento',
        border: true
    }

    return menu(descuentosDisponibles, configuracionesDelMenu)
        .then((item) => {
            let indice = 1
            while (indice <= descuentosDisponibles.length) {
                switch (item.hotkey) {
                    case indice:
                        objetoReporte += `, Descuento: ${item.title}`
                        reportes.push(objetoReporte)
                        usuario.estado = false
                        menuPrincipal(usuariosCreados, usuario) //usuariosCreados,
                        break;
                }
                indice++
            }
        })
}





// creacion de elemetos
const crearUsuario = (usuarios, ListaReportes) => {
    usuarios.forEach(valor=>{
        misUsuarios.push(valor)
    })
    if (existenElemetos(usuarios)) {
        menuPrincipal(usuarios, ListaReportes)
    } else {
        menuPrincipal()
    }


}

const crearProveedor = (proveedores, usuario) => {        
    proveedores.forEach(valor=>{
        misProveedores.push(valor)
    })
    if (existenElemetos(misProveedores)) {
        menuUsuarioLogueado(misProveedores, usuario)
    } else {
        menuUsuarioLogueado()
    }


}

const crearDescuento = (descuento, Proveedor, usuario) => {
    Proveedores[Proveedor.hotkey - 1].descuentos.push(descuento)
    if (existenElemetos(Proveedores[Proveedor.hotkey - 1].descuentos)) {
        menuProveedor(Proveedor, usuario)
    } else {
        menuProveedor()
    }


}

// logue
const logearUsuario = (usuario) => {    
    console.log(`Nombre: ${usuario.title}`)
    usuario.estado = true
    objetoReporte = `Nombre: ${usuario.title}`
    console.log(misProveedores)        
    menuUsuarioLogueado(misProveedores, usuario)   
}


// funciones del vector
var funcionDeListar = (arreglo) => {
    arreglo.forEach((valor) => {
        console.log(valor)
    })
}

var existenElemetos = (arreglo) => {
    let hayElemetos = arreglo.length > 0
    if (hayElemetos) {
        return true
    } else {
        return false
    }
}

menuPrincipal()




// readlineSync.promptCL(function (userName) {
//     let nuevoUsuario = {}
//     nuevoUsuario.nombre = userName
//     nuevoUsuario.estadoDeLogueo = item.data.estadoDeLogueo)}