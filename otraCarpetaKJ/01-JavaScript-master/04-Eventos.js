const EventEmitter = require('events');

///// evento principla
class entrarALaCasa extends EventEmitter{}

const entrarCasa = new entrarALaCasa();

/// evento que escuchan
entrarCasa.on('entra por la puerta delantera',(nombre)=>{
    console.log(`saludar a ${nombre}, que ingrerso por la puerta delantera`)
})


entrarCasa.on('entra por la puerta de atras',(nombre)=>{
    console.log(`saludar a ${nombre}, que ingrerso por la puerta de atras`)
})


entrarCasa.on('entra por la ventana',()=>{
    console.log(`cuidado entro por la ventana`)
})



//// evento que emite
entrarCasa.emit('entra por la puerta delantera','kevin')

entrarCasa.emit('entra por la puerta de atras','kevin')

entrarCasa.emit('entra por la ventana')

//////////////////////////  mi evento

class comprar extends EventEmitter{}

comprarCosas = new comprar();

comprarCosas.on('comprar viveres',(nombre)=>{
   console.log(`el cliente adquirio el siguiente vivere: ${nombre} `)
})

comprarCosas.on('comprar ropa',(nombre)=>{
   console.log(`el cliente adquirio la prenda: ${nombre} `)
})

comprarCosas.on('comprar comida',(nombre)=>{
   console.log(`el cliente adquirio la comida: ${nombre} `)
})

comprarCosas.on('comprar dulces',(nombre)=>{
    console.log(`el cliente adquirio el dulce: ${nombre} `)
})


comprarCosas.emit('comprar viveres','leche')
comprarCosas.emit('comprar ropa','zapatos')
comprarCosas.emit('comprar comida','jugo')
comprarCosas.emit('comprar dulces','doritos')

