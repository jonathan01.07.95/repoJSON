

module.exports = (arreglo,usuarioABuscar,cb)=>{
    
    
    const usuarioEncontrado = arreglo.find(usuario=>{   
        let existeElUsuario = (usuario.nombre === usuarioABuscar.nombre&& usuario.apellido === usuarioABuscar.apellido)                
        return existeElUsuario
    })
    if (usuarioEncontrado) {
        cb({
            mensaje: 'usuario encontrado',
            usuarioEncontrado
        })
    }else{
        cb({
            mensaje: 'usuario no encontrado',
            usuarioEncontrado
        })
    }
}