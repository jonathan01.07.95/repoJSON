const paquetes = require('./paquetes.js')
module.exports = (arreglo,usarioAbuscarOCrear,cb)=>{
    paquetes.buscarUsuario(arreglo,usarioAbuscarOCrear,(respuestaDelCallbackDeBuscarUsuario)=>{
        if (respuestaDelCallbackDeBuscarUsuario.usuarioEncontrado) {
            paquetes.eliminarUsuario(arreglo,respuestaDelCallbackDeBuscarUsuario.usuarioEncontrado,(respuestaDelCallbackDeEliminarUsuario)=>{
                cb({
                    mensaje: 'se elimino el usuario',
                    usuario: respuestaDelCallbackDeEliminarUsuario.arreglo
                })
            })            
        } else {    
            paquetes.crearUsuario(arreglo,usarioAbuscarOCrear,(respuestaDelCallbackDeCrearUsuario)=>{
                cb({
                    mensaje: respuestaDelCallbackDeCrearUsuario.mensaje,
                    arreglo
                })
            })            
        }
    })

}