module.exports = (arreglo,usuarioAEliminar,cb)=>{
    let existeElUsuario = (usuario => (usuario.nombre === usuarioAEliminar.nombre && usuario.apellido === usuarioAEliminar.apellido))
    const indiceDelElementoABorrar = arreglo.findIndex(existeElUsuario)
    arreglo.splice(indiceDelElementoABorrar,1)
    cb({
        mensaje: 'se elimino con exito',
        arreglo
    })
}



