// funcione asincronas
// sin usi de callback
function saludar(saludo){
    console.log('ejecuto saludo', saludo)
    return saludo
}

function peticionUsuario(saludo,cb){
    cb(saludo)    
}
// para poder realozar callbac se llana la funcion sin parentesis y dentro
// de la peticion llamra a la funcion
peticionUsuario('hola',saludar)

// repuesta del callback
peticionUsuario('hola',(saludar)=>{
    console.log('respuesta',saludar)
})

///////////////////////////

const leerArchivo = require('./leerAchivo')

function peticionUsuario2(){
    console.log('inicio Peticion')

    leerArchivo('texto6.txt',(respuestaJson)=>{
        if (respuestaJson.errorCreado) {
            console.log('Archivo creado con la info',respuestaJson.contenido)
        }else{
            console.log('Archivo no creado')
        }
    },'hola bro')


    console.log('termino Peticion')
}

peticionUsuario2()


//buscar un usuario, se busca si no encuentra se lo crea, verificacion en su tiene nombre y appelido, no se crea si no tine los 2 usaios
//funcion para ccrear usuario