

const fs = require('fs')

module.exports = (path,cb,info)=>{
    fs.readFile(path, 'utf8', (errorLectura,informacion)=>{        
        if(errorLectura){
            fs.writeFile(path, info, (error)=>{                                
                if (error) {
                    cb({
                        error: error,
                        contenido: info,
                        errorCreado: false
                    })
                }else{
                    cb({
                        error: error,
                        contenido: info,
                        errorCreado: true
                    })
                }
            })
        }else{
            cb({
                error: errorLectura,
                contenido: informacion,
                leido: true                
            })// termina 
        }
        
    });
        }

                                        