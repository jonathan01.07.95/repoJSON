//   https://www.londonappdeveloper.com/consuming-a-json-rest-api-in-android/

// http://programmerguru.com/android-tutorial/android-facebook-login-implementation-with-fragment/


// var fecha = new Date()

// console.log(fecha+ 'iso string')

// console.log('anio: ',fecha.getFullYear())

// console.log('mes: ',fecha.getMonth()) //los meses toma del 0 a 11

// console.log('dia: ',fecha.getDay()) // numeo del dia, comenzando desde el domingo = 0

// console.log('dia numero: ',fecha.getDate())

// var fechaNacimiento = new Date('1995-02-24')
// console.log(fechaNacimiento.getFullYear())

// var edadActual = (anioNacimiento)=>{
//     let edad = new Date().getFullYear()-anioNacimiento  
//     return edad
// }

// console.log(edadActual(fechaNacimiento.getFullYear()))


// parseInt = string a number

// toString = number a string

//totimesatring



/********************************************** */
/// moment js

// var moment = require('moment');
// moment.locale('es') // para el idioma

// var fecha = moment().format() //pareceido al iso string, 
// var hora = moment().format('LTS') // para solo la hora solo ('LT') y para segnundo aunmentar la S

// var start = moment().startOf('day').fromNow(); // cuanto a pasado hasta este momento
// var end = moment().endOf('day').fromNow();   // cuanto falta para que se acabe el dia

// var fecha = moment() // obtener dias, mes, año

// //año
// console.log(fecha.get('year')) // se usa en funciones reutilizables
// console.log(fecha.year())


// //mes
// console.log(fecha.get('month'))
// console.log(fecha.month())


// //dia
// console.log(fecha.get('date'))
// console.log(fecha.date())


// /************************** */




// var jsonFecha = {
//     year:2018,
//     month: 7,
//     day: 25
// }

// var fecha = moment().toObject()

// console.log(fecha)


// var fechaInicial = moment('1995-02-24')
// var fechaFinal = moment('2018-12-24')
// var diferencia = console.log('diff',fechaFinal.diff(fechaInicial,'years')) // restar entre fecahs indicando lo que se reuiere

// array de objetos de nombres, add fecha de nacimiento, rango 1940-2018, calcular la edad-fincion aparte-cuales son menores de edad
// < 18
// mas de 18 - 55
// < 55 3ra edad

const moment = require('moment');
const random = require('random-js')();
moment.locale('es')


var edadActual = (fechaNacimiento) => {
    let fechaActual = moment()
    let edad = fechaActual.diff(fechaNacimiento,'years')
    return edad
}


var misUsuarios = [
    {
        nombre: 'kevin'
    },
    {
        nombre: 'orlando'
    },
    {
        nombre: 'katherine'
    },
    {
        nombre: 'paulina'
    },
    {
        nombre: 'daniela'
    },
    {
        nombre: 'brenda'
    },
    {
        nombre: 'mandy'
    },
    {
        nombre: 'paul'
    },
    {
        nombre: 'ryan'
    },
    {
        nombre: 'juan'
    }
]
var mayoresA55 = []
var estaEntre18Y55 = []
var menoresA18 = []

var añadirAtributos = misUsuarios.map(valor => {
    let año = random.integer(1940, 2018)
    let mes = random.integer(0, 11)
    let dia = random.integer(1, 31)
    let jsonFecha = {
        year: año,
        month: mes,
        day: dia
    }
    let fecha = moment(jsonFecha)
    valor.fechaDeNacimiento = fecha.format('L')
    valor.edad = edadActual(fecha)
    return valor
})

añadirAtributos.reduce((acumulador,valor)=>{
    let menor18= valor.edad <= 18
    if(menor18){
        acumulador.push(valor)
    }
    return acumulador
},menoresA18)

añadirAtributos.reduce((acumulador,valor)=>{
    let esta18A55= valor.edad > 18 && valor.edad <55
    if(esta18A55){
        acumulador.push(valor)
    }
    return acumulador
},estaEntre18Y55)

añadirAtributos.reduce((acumulador,valor)=>{
    let mayor55 = valor.edad >= 55
    if(mayor55){
        acumulador.push(valor)
    }
    return acumulador
},mayoresA55)
//console.log(addAtributos)
console.log('menores a 18',menoresA18)
console.log('****************************************************')
console.log('esta entre 18 y 55',estaEntre18Y55)
console.log('****************************************************')
console.log('mayores a 55',mayoresA55)






