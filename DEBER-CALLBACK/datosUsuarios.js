module.exports.arregloUsuarios = [
    {
        nombre: 'Ronaldinho'
    },
    {
        nombre: 'Messi'
    },
    {
        nombre: 'Riquelme'
    }
]

module.exports.usuarioAIngresar = {
    nombre: 'Ronaldo'
}

module.exports.usuarioABuscar = {
    nombre: 'Kaka'
}

module.exports.usuarioAEliminar = {
    nombre: 'Juan'
}

module.exports.usuarioBuscarCrear = {
    nombre: 'Penaldo'
}