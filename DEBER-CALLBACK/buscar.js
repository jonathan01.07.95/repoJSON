module.exports = (arregloUsuarios, usuarioABuscar, cb)=>{
    const usuarioEncontrado = arregloUsuarios.find((usuario)=>{
        return usuario === usuarioABuscar
    })

    if(usuarioEncontrado){
        cb({
            mensaje: 'usuario encontrado',
            usuarioEncontrado
        })
    
    }
    else{
        cb({
            mensaje: 'usuario no encontrado',
            usuarioEncontrado
        })
    }
}
