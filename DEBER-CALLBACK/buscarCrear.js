//buscarOCrear
const paquetes = require('./paquetes.js')
module.exports = (arregloUsuarios, usuarioABuscarOCrear, cb)=>{
    paquetes.buscar(arregloUsuarios, usuarioABuscarOCrear, (respuestaBuscar)=>{
        if(respuestaBuscar.usuarioEncontrado){
            cb({
                mensaje:'se encontro el usuario',
                usuarioEncontrado: respuestaBuscar.usuarioEncontrado
            })
        }
        else{
            paquetes.crear(arregloUsuarios,usuarioABuscarOCrear,(respuestaCrearUsuario)=>{
                cb({
                    mensaje:'usuario creado',
                    arregloUsuarios
                })
            })
        }
    })
    cb({
         mensaje: 'si vale',
         arregloUsuarios
    })
    }