const crear = require('./crear.js') //Cualquiera de las dos maneras 
const buscar = require('./buscar.js')
const eliminar = require('./elimnar.js')
const buscarCrear = require('./buscarCrear.js')
const usuarios = require('./datosUsuarios.js')

module.exports = {
    crear,
    buscar,
    eliminar,
    buscarCrear,
    usuarios
}