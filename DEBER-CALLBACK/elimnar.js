const paquetes = require('./paquetes.js')

module.exports = (arregloUsuarios, usuarioAEliminar, cb) => { 
    paquetes.buscar(arregloUsuarios, usuarioAEliminar, (respuestaBuscarEliminar)=>{
        if(respuestaBuscarEliminar.usuarioEncontrado){
            eliminar(arregloUsuarios, usuarioEncontrado, (respuestaEliminar)=>{
                cb({
                    mensaje: 'se encontro el usuario a eliminar',
                    arregloUsuarios: respuestaEliminar.arregloUsuarios,
                })
            })
        }
        else{
            cb({
                mensaje: 'no se encontro el usuario que se desea a eliminar',
                arregloUsuarios
            })
        }
    })
}

function eliminar(arregloUsuarios, usuario, cb) {
    var indice = arregloUsuarios.indexOf(usuario)
    console.log('valor indice', indice)
    if(indice > -1){
        arregloUsuarios.splice(indice, 1)
    }
    cb({
        mensaje: 'se elimino el usuario',
        arregloUsuarios
    })
}

