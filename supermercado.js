var readlineSync = require('readline-sync');
var menu = require('console-menu');

console.log('Bienvenidos');
console.log('Seleccione una opcion de menu');

const arregloUsuario = []

const registrarUsuario = function (datos) {
    console.log('Datos ', datos);
    arregloUsuario.push(datos);
    const hayUsuarios = arregloUsuario.length > 0

    if(hayUsuarios){
        ejecutarMenu(true)
    }else {
        ejecutarMenu()
    }

}

const logearUsuario = function (datos) {
    console.log('Datos ', datos);
    console.log(arregloUsuario);
    seleccionarUsuario(arregloUsuario)
}

const seleccionarUsuario = (usuarios)=>{
    const hayUsuarios = usuarios.length > 0

    if(hayUsuarios){
        ejecutarMenuUsuarios(usuarios)
    }else {
        ejecutarMenu()
    }
}

const ejecutarMenu = (agregarOpcion) => {

    const menuSeleccion =[
        { hotkey: '1', title: 'Crear usuario', data: { nombre: 'Adrian' } },
        { hotkey: '2', title: 'Logear usuario', data: { nombre: 'Vicente' } }
    ]


    if(agregarOpcion){
        menuSeleccion[2]= { hotkey: '3', title: 'Listar usuarios', data: { nombre: 'Adrian' }}
    }


    return menu(menuSeleccion, {
            header: 'Menu',
            border: true,
        })
        .then(item => {
            switch (item.hotkey) {
                case '1':
                    console.log('Opcion 1');
                    registrarUsuario(item.data);
                    break;
                case '2':
                    console.log('Opcion 2');
                    logearUsuario(item.data);
                    break;
                    case '3':
                    console.log('Opcion 3');
                    ejecutarMenuUsuarios(arregloUsuario)
                    break;
                default:
                    console.log('Opcion invalida');
            }
        });
}

ejecutarMenu();