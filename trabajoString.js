var parrafo = 'El Nacional tiene la segunda peor defensa en lo que va del Campeonato. El equipo de los puros criollos recibió 45 goles, algunos determinantes en sus derrotas y empates. Solo el Técnico Universitario ha encajado más tantos en las 27 fechas disputadas. El ‘Rodillo’ vio caer su arco en 49 ocasiones y es último en la tabla acumulada de posiciones. Los criollos reconocen que los problemas para marcar son una de sus debilidades en el torneo y que mantienen al club en novena posición, entre los 12 equipos. “Tenemos cosas que prevenir en los partidos”, admitió Javier Quiñónez, quien es titular en el centro de la defensa del plantel, junto a Luis Segovia.'

var primeraPalabraMayusculas = () =>{
    var palabraMayusculaInicial = parrafo.slice(0, parrafo.indexOf(' '))
    return palabraMayusculaInicial.toUpperCase()
}

console.log('La primera palabra en mayusculas es: ' + primeraPalabraMayusculas())

var ultimaPalabraMayusculas = () =>{
    var palabraMayusculaFinal = parrafo.slice(parrafo.lastIndexOf(' ')+1, parrafo.length-1)
    return palabraMayusculaFinal.toUpperCase()
}

console.log('La ultima palabra en mayusculas es: ' + ultimaPalabraMayusculas())

/*var palabraMedioMayusculas = () =>{
    var BuscarPalabra = separarPalabras(' ')[(palabrasSeparadas.length/2)+0.5]
    return BuscarPalabra
}

console.log('La palabra del Medio en Mayusculas es: ' + palabraMedioMayusculas())
*/

console.log('La palabra del medio: ' + separarPalabras(' '))

var separarPalabras = (parametro) =>{
    var palabrasSeparadas = separarPalabras(' ')
    return palabrasSeparadas
}

var contarPuntos = () =>{
    var numeroPuntos = separarPalabras('. ').length
    return numeroPuntos
}

console.log('El numero de puntos es: ' + contarPuntos())

var separarPuntosYGuardarNuevo = () =>{
    var fraseNuevaSeparada = separarPalabras('. ')
    return fraseNuevaSeparada
}

console.log(separarPuntosYGuardarNuevo())

/*
var parrafo = 'El Nacional tiene la segunda peor defensa'

var cambiarEspacios = () =>{
    var i=0
    while(i<parrafo.length){
        i++
        var nuevoParrafo = parrafo.replace(' ', '*-*')     
    }
    return nuevoParrafo
}

console.log(cambiarEspacios())
*/