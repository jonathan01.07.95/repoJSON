/*
// round, ceil, floor

var numero1 = 3.2
var numero2 = 3.8
var numero3 = 0.7
var numero4 = 0.9
var numero5 = 4.7741
var numero6 = 4.499999999999999999999999999999999999999999999
var numero7 = -4.5
var numero8 = -4.6


console.log(Math.round(numero1))
console.log(Math.round(numero2))
console.log(Math.round(numero6))
console.log(Math.round(numero7))
console.log(Math.round(numero8))

console.log(`ceil`, Math.ceil(numero1))
console.log(`ceil`, Math.ceil(numero2))
console.log(`ceil`, Math.ceil(numero6))
console.log(`ceil`, Math.ceil(numero7))
console.log(`ceil`, Math.ceil(numero8))

console.log(`floor`, Math.floor(numero1))
console.log(`floor`, Math.floor(numero2))
console.log(`floor`, Math.floor(numero6))
console.log(`floor`, Math.floor(numero7))
console.log(`floor`, Math.floor(numero8))

// random

console.log(`randomicos`, Math.round(Math.random())*100)
console.log(`randomicos`, Math.round(Math.random())*100)

// maximo

console.log(Math.max(1,45,2,6,92,5))

// minimo

console.log(Math.min(1,45,2,6,92,5))

// enviar como paramteros separados --> ... para que me lea el vector

console.log(Math.max(...[1,45,2,6,92,5]))

// potenciacion
console.log(Math.pow(2,4))
*/

///////////
/*
var arreglo = [2,4,6,3,56]

// every todos cumplan, some alguno exista

var resultadoEvery = arreglo.every(valor =>{
    return valor === 'number'
})

console.log(`resultado every`, resultadoEvery )

var resultadoSome = arreglo.some((valor, indice) =>{
    var esMismoTipo = typeof valor === 'number'
    if (esMismoTipo){
        console.log(`valor e indice`, valor, indice)
        return true
    }
})

console.log(`resultado some`, resultadoSome)

var resultadoSumaArreglo = () => {
    var sumaElementos = 0
    for(let i = 0; i < arreglo.length; i++){
        sumaElementos = sumaElementos + arreglo[i]
    }
    arreglo.forEach((valor) => {
        sumaElementos += valor
    });
    return sumaElementos
}
*/
/*
console.log(`La suma es`, resultadoSumaArreglo())

var resultadoConReduce = arreglo.reduce((acumulador, valor,  indice ) => {
    if(indice%2 == 0){
        acumulador += valor
    }
    return acumulador
}, 0)

console.log(`resultado con reduce`, resultadoConReduce)
*/
/////////////////////////////////

//nombre, sexo del dueño y nombre de la mascota
//reduce solo sexo masculino

var propietarios = [
    {
        nombrePropietario: 'Jonathan',
        sexoPropietario: 'Masculino',
        nombreMascota: 'Maggie'
    },
    {
        nombrePropietario: 'Kevin',
        sexoPropietario: 'Masculino',
        nombreMascota: 'Perra'
    },
    {
        nombrePropietario: 'Patricia',
        sexoPropietario: 'Femenino',
        nombreMascota: 'Pollo1'
    },
    {
        nombrePropietario: 'Alisson',
        sexoPropietario: 'Femenino',
        nombreMascota: 'Shadok'
    }
]

var sumaEdades = (a,b) =>{
    return a+b
}

var anadirEdades = propietarios.map((valor) =>{
    valor.edadHumana = Math.round(Math.random()*100)
    valor.edadHMascota = Math.round(Math.random()*10)
    valor.sumaEdades = sumaEdades(valor.edadHumana, valor.edadHMascota)
    return valor
})
.filter((valor, index) => {
    let esMasculino = valor.sexoPropietario === 'Masculino'
    let esMenor20 = valor.edadHumana <= 20
    if(esMasculino && esMenor20){
        return valor
    }
})

console.log(anadirEdades)













/*
var acumuladorDueños = propietarios.reduce((acumulador, valor)=>{/*
    
    var esMasculino = valor.sexoPropietario === 'Masculino'
   // var edadDueño = valor.edadDueño < 20
    if(esMasculino){
        acumulador.push(valor)
    }
    return acumulador
}, [] )

console.log(`acumulador dueño con reduce`, acumuladorDueños)
*/

// agregar 3 atributos nuevos edad: humano y edad mascota -->
// suma de las mascotas funcion filtrar los que son masculinos y 
// si hay edades de los dueños < 20