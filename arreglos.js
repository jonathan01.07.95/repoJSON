var arregloNumeros = [1,2,3,4,5]

console.log(arregloNumeros.length)
console.log(arregloNumeros.lastIndexOf(3))

console.log(arregloNumeros)
console.log(arregloNumeros.push(6))
console.log(arregloNumeros)
console.log(arregloNumeros[3])

// slice, splice (corto al vector con diferente logica)

//var arregloSlice = arregloNumeros.slice(2,4)
//console.log(arregloSlice)

//var arregloSplice = arregloNumeros.splice(1, 2)
console.log(arregloNumeros)
//console.log(arregloSplice)
console.log(arregloNumeros)
console.log(arregloNumeros.pop())
console.log('original despues del plop', arregloNumeros)
// como listar con for pero no se puede hacer con el for 

for(let i = 0; i < arregloNumeros.length; i++){
    console.log(arregloNumeros[i])
}

arregloNumeros.forEach((valor, indice, arreglo)=>{
    console.log('valor', valor)
    console.log('indice', indice)
    console.log('vector', arreglo)
});

var operadorMap = arregloNumeros.map((valor, indice)=>{
    //return valor + 7
    if(indice % 2 === 0){
        valor +=7
    }
    return valor
})

console.log(arregloNumeros)
console.log(operadorMap)

var operadorFilter = arregloNumeros.filter((valor, indice) =>{
    return valor === 3
})

console.log(arregloNumeros)
console.log(operadorFilter)

var operadorFind = arregloNumeros.find((valor, indice)=>{
    return valor === 5
})

console.log(operadorFind)

//concatenar

//arregloNumeros.map().forEach()