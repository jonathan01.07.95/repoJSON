class Persona{ 
    constructor(protected nombre?:string, protected apellido?: string, protected domicillio?: string, 
        protected CI?: number, protected profesion?: string){
        this.nombre = nombre;
        this.apellido = apellido;
        this.domicillio = domicillio;
        this.CI = CI;
        this.profesion = profesion;
    }

    public setNombre(nombre: string){ 
        this.nombre = nombre; 
    } 
            
    public getNombre(){ 
        return this.nombre; 
    } 

    public setApellido(apellido: string){ 
        this.apellido = apellido; 
    } 
            
    public getApellido(){ 
        return this.apellido; 
    } 

    public setDomicillio(domicillio: string){ 
        this.domicillio = domicillio; 
    } 
            
    public getDomicillio(){ 
        return this.domicillio; 
    } 

    public setCI(CI: number){ 
        this.CI = CI; 
    } 
            
    public getCI(){ 
        return this.CI; 
    } 

    public setProfesion(profesion: string){ 
        this.profesion = profesion; 
    } 
            
    public getProfesion(){ 
        return this.profesion; 
    } 
}


class Estudiante extends Persona{
    constructor(public nombre, public apellido, public domicillio, public CI, public profesion, 
        public universidad, public carrera, public semestreActual){
        super(nombre, apellido, domicillio, CI, profesion);
        universidad = universidad;
        carrera = carrera;
        semestreActual = semestreActual;
    }
}

const estudiante = new Estudiante('Juan', 'Lopez', 'Conocoto', 1720446218,'Estudiante', 'EPN', 'Sistemas y Computacion', '6to' )
console.log(estudiante)

class TrabajadorSupermaxi extends Persona{
    constructor(public nombre, public apellido, public domicillio, public CI, public profesion, 
        public fechaInicio, public cargo){
        super(nombre, apellido, domicillio, CI, profesion);
        fechaInicio = fechaInicio;
        cargo = cargo;
    }
}

const trabajadorSupermaxi = new TrabajadorSupermaxi('Jonathan', 'Parra', 'Conocoto', 1720446218, 'Trabajador', 'Supermaxi', 'Cajero' )
console.log(trabajadorSupermaxi)

class Bombero extends Persona{
    constructor(public nombre, public apellido, public domicillio, public CI, public profesion, 
        public rango, public distrito){
        super(nombre, apellido, domicillio, CI, profesion);
        rango = rango;
        distrito = distrito;
    }
}

const bombero = new Bombero('Jonathan', 'Parra', 'Conocoto', 1720446218, 'Bombero', 'Teniente', 'Valle de los Chillos' )
console.log(bombero)