// typescript
// ; en caso de que se requiera
//exportar la variable 

const nombre: string = 'Cristian';
const numero: number = 3;
const estado: boolean = true;
let a:number;
const objetoPersona = {
    nombre: 'test'
}
const arreglo: Array<number | string > = []
const arreglo2: (number | string)[] = []

arreglo.push('4')
let cualquierVariable: any 

function sumar(a?:number, b?:number): number {
    let a1 = 2;
    let b1 = 5;
    if(a){
        a1 = a;
    }
    if(b){
        b1 = b;
    }
    return a+b;
}

const restar = (): number =>{
    return 4
}

sumar(2,5);
sumar(3);
sumar();

// variables privadas siempre con guion bajo

class PersonaClase{
    
/*     private _nombre: string;
    private _apellido: string; */

    constructor(protected nombre?:string, protected apellido?: string){
        this.nombre = nombre;
        this.apellido = apellido;
    }


}

const persona = {
    nombre: 'test',
    apellido: 'test'
}

// para compilar con tsc nuevo.ts -t es2017
const personaClase = new PersonaClase('clase nombre','clase apellido');

console.log('persona', persona)
console.log('persona clase', personaClase)

const persona3: PersonaClase = new PersonaClase()

//persona3.nombre('Peppa');

class Hijo extends PersonaClase{

    constructor(public nombre, public apellido, public apellidoMaterno){
        super(nombre, apellido);
        apellidoMaterno = apellidoMaterno;
    }
}

const hijoVariable = new Hijo('Juan', 'Lopez', 'Perez')

console.log(hijoVariable)