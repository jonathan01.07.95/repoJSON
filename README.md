/*
function saludar(){
    console.log("empezo a saludar")
    return "hola" 
}

saludar();
//console.log(saludar())

function despedirse(){
    saludar();
    return "adios"
}

console.log(despedirse())


var funcionAnonima = function (){
    return "funcion anonima"
}

funcionAnonima()

var funcionFlecha = ()=>{
    return "funcion flecha"
}

console.log(funcionAnonima())
console.log(funcionFlecha())

var sumar = (a,b)=>{
    return a+b
}

console.log(sumar(4,7))
let f = true --> variable local
var --> variable global
cons --> variable constante

*/

var a = 10
var b = 2
var json = {
    sumar : (a,b)=>{
        return a+b
    },
    restar : (a,b)=>{
        return a-b
    },
    multiplicar : (a,b)=>{
        return a*b
    },
    dividir : (a,b)=>{
        return a/b
    },
    modulo : (a,b)=>{
        return a%b
    }
}

console.log("La suma: " + json.sumar(a,b) + "\nLa resta: " + json.restar(a,b) +
"\nLa multiplicacion es: " + json.multiplicar(a,b)+
"\nLa division es: " + json.dividir(a,b)+
"\nEl modulo es: " + json.modulo(a,b))